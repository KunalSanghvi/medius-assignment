from flask import Flask, render_template, request
import pandas as pd
import pymysql

app = Flask(__name__)
connection = pymysql.connect(host='localhost',user='root',password='server@',db='medius')
cursor=connection.cursor()

@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')

@app.route('/data', methods=['GET', 'POST'])
def data():
    if request.method == 'POST':
        file =  request.form['upload-file']
        sheet1 = pd.read_excel(file, sheet_name = 0)
        cols = "`,`".join([str(i) for i in sheet1.columns.tolist()])
        for i,row in sheet1.iterrows():
            sql = "INSERT INTO `customer` (`" +cols + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
            cursor.execute(sql, tuple(row))
            connection.commit()
        sheet2 = pd.read_excel(file, sheet_name = 1)
        cols = "`,`".join([str(i) for i in sheet2.columns.tolist()])
        for i,row in sheet2.iterrows():
            sql = "INSERT INTO `branchdata` (`" +cols + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
            cursor.execute(sql, tuple(row))
            connection.commit()
        sheet3 = pd.read_excel(file, sheet_name = 2)
        cols = "`,`".join([str(i) for i in sheet3.columns.tolist()])
        for i,row in sheet3.iterrows():
            sql = "INSERT INTO `customerhomeaddressdata` (`" +cols + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
            cursor.execute(sql, tuple(row))
            connection.commit()
        sheet4 = pd.read_excel(file, sheet_name = 3)
        cols = "`,`".join([str(i) for i in sheet4.columns.tolist()])
        for i,row in sheet4.iterrows():
            sql = "INSERT INTO `customerofficedata` (`" +cols + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
            cursor.execute(sql, tuple(row))
            connection.commit()
        sheet5 = pd.read_excel(file, sheet_name = 4)
        cols = "`,`".join([str(i) for i in sheet5.columns.tolist()])
        for i,row in sheet5.iterrows():
            sql = "INSERT INTO `loanamountdata` (`" +cols + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
            cursor.execute(sql, tuple(row))
            connection.commit()
        return render_template('data.html', data=data)

if __name__ == '__main__':
    app.run(debug=True)