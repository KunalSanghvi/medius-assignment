create table Customer(
	LoanAccountNo int not null,
    CustomerName varchar(255) not null,
    FatherName varchar(255) not null,
    CustomerProfile varchar(255) not null,
    primary key (LoanAccountNo)
);

create table BranchData(
	BranchName varchar(255) not null,
    BranchCode varchar(255) not null,
    AreaName varchar(255) not null,
    RegionName varchar(255) not null,
    ZoneName varchar(255) not null,
    LoanAccountNo int,
    primary key (BranchCode),
    foreign key (LoanAccountNo) references Customer(LoanAccountNo)
);

create table CustomerHomeAddressData(
	Address1 varchar(255) not null,
    Address2 varchar(255),
    Address3 varchar(255),
    Landmark varchar(255),
    Pincode int,
    LoanAccountNo int,
    foreign key (LoanAccountNo) references Customer(LoanAccountNo)  
);

create table CustomerOfficeData(
	OfcAddress1 varchar(255) not null,
    OfcAddress2 varchar(255),
    OfcAddress3 varchar(255),
    OfficeLandmark varchar(255),
    OfficePincode varchar(255),
    LoanAccountNo int,
    foreign key (LoanAccountNo) references Customer(LoanAccountNo)
);

create table LoanAmountData(
	AgreementDate date not null,
    LRN int,
    Tenor varchar(255) not null,
    AdvEMI int,
    MOB varchar(255),
    LoanAccountNo int,
    foreign key (LoanAccountNo) references Customer(LoanAccountNo)
);